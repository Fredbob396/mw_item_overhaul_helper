import subprocess
import config
import os
import json


class Tes3cmd:
    esps = []
    original_wd = os.getcwd()

    weapon_dump_format = \
        "{" \
        "\"csid\":\"%id%\"," \
        "\"name\":\"%name%\"," \
        "\"type\":\"%type%\"," \
        "\"model\":\"%model%\"," \
        "\"weight\":%weight%," \
        "\"value\":%value%," \
        "\"health\":%health%," \
        "\"speed\":%speed%," \
        "\"reach\":%reach%," \
        "\"enchantment\":%enchantment%," \
        "\"chop_min\":%chop_min%," \
        "\"chop_max\":%chop_max%," \
        "\"slash_min\":%slash_min%," \
        "\"slash_max\":%slash_max%," \
        "\"thrust_min\":%thrust_min%," \
        "\"thrust_max\":%thrust_max%," \
        "\"enchanting\":\"%enchanting%\"" \
        "},"

    armor_dump_format = \
        "{" \
        "\"csid\":\"%id%\"," \
        "\"name\":\"%name%\"," \
        "\"type\":\"%type%\"," \
        "\"model\":\"%model%\"," \
        "\"weight\":%weight%," \
        "\"value\":%value%," \
        "\"health\":%health%," \
        "\"enchantment\":%enchantment%," \
        "\"ar\":%ar%," \
        "\"enchanting\":\"%enchanting%\"" \
        "},"

    clothing_dump_format = \
        "{" \
        "\"csid\":\"%id%\"," \
        "\"name\":\"%name%\"," \
        "\"type\":\"%type%\"," \
        "\"model\":\"%model%\"," \
        "\"weight\":%weight%," \
        "\"value\":%value%," \
        "\"enchantment\":%enchantment%," \
        "\"enchanting\":\"%enchanting%\"" \
        "},"

    weapon_types = {
        0: "ShortBladeOneHand",
        1: "LongBladeOneHand",
        2: "LongBladeTwoClose",
        3: "BluntOneHand",
        4: "BluntTwoClose",
        5: "BluntTwoWide",
        6: "SpearTwoWide",
        7: "AxeOneHand",
        8: "AxeTwoHand",
        9: "MarksmanBow",
        10: "MarksmanCrossbow",
        11: "MarksmanThrown",
        12: "Arrow",
        13: "Bolt"
    }

    armor_types = {
        0: "Helmet",
        1: "Cuirass",
        2: "Left_Pauldron",
        3: "Right_Pauldron",
        4: "Greaves",
        5: "Boots",
        6: "Left_Gauntlet",
        7: "Right_Gauntlet",
        8: "Shield",
        9: "Left_Bracer",
        10: "Right_Bracer"
    }

    clothing_types = {
        0: "Pants",
        1: "Shoes",
        2: "Shirt",
        3: "Belt",
        4: "Robe",
        5: "Right_Glove",
        6: "Left_Glove",
        7: "Skirt",
        8: "Ring",
        9: "Amulet"
    }

    @classmethod
    def __init__(cls, esps):
        # Check if all .esp/.esm files exist
        for esp in esps:
            esp_path = config.mw_data_files_dir + "\\" + esp
            if not os.path.isfile(esp_path):
                raise FileNotFoundError
        # Remember .esps for command running
        cls.esps = esps

    @classmethod
    def get_initial_weapons_dump(cls):
        weapons_dump = cls.run_initial_dump("WEAP", cls.weapon_dump_format)
        # Adjust data
        for esp, dump in weapons_dump.items():
            for weapon in dump:
                weapon["type"] = cls.weapon_types[int(weapon["type"])]
                weapon["weight"] = round(weapon["weight"], 2)
                weapon["speed"] = round(weapon["speed"], 2)
                weapon["reach"] = round(weapon["reach"], 2)
        return weapons_dump

    @classmethod
    def get_initial_armor_dump(cls):
        armor_dump = cls.run_initial_dump("ARMO", cls.armor_dump_format)
        # Adjust data
        for esp, dump in armor_dump.items():
            for armor in dump:
                armor["type"] = cls.armor_types[int(armor["type"])]
                armor["weight"] = round(armor["weight"], 2)
        return armor_dump
    
    @classmethod
    def get_initial_clothing_dump(cls):
        clothing_dump = cls.run_initial_dump("CLOT", cls.clothing_dump_format)
        # Adjust data
        for esp, dump in clothing_dump.items():
            for clothing in dump:
                clothing["type"] = cls.clothing_types[int(clothing["type"])]
                clothing["weight"] = round(clothing["weight"], 2)
        return clothing_dump

    @classmethod
    def run_initial_dump(cls, record_type, dump_format):
        # Change to Data Files directory for running commands
        os.chdir(config.mw_data_files_dir)
        full_dump = {}
        for esp in cls.esps:
            # Run the tes3cmd process
            proc = subprocess.Popen(
                ["tes3cmd.exe", "dump",    # Run the dump command
                 "--type", record_type,    # Only of the current type
                 "--format", dump_format,  # JSON format string
                 "--no-quote",             # Don't quote data with spaces
                 esp],                     # Point from the current .esp
                stdout=subprocess.PIPE,
                shell=True,
                creationflags=subprocess.CREATE_NO_WINDOW)
            (out, err) = proc.communicate()
            # Convert pseudo-JSON into a proper JSON object
            current_dump = cls.parse_dump_json_string(out.decode("utf-8"))
            # Add dump to dict with the .esp as the key
            full_dump[esp] = current_dump
        # Return to original directory after running commands
        os.chdir(cls.original_wd)
        return full_dump

    @classmethod
    def parse_dump_json_string(cls, dump):
        result = "{\"data\":["
        result += dump
        result = result.replace('\n', ' ').replace('\r', '')
        result = result.replace('\\', '\\\\')
        result = result[:-2]
        result += "]}"
        json_result = json.loads(result)
        return json_result["data"]

    @classmethod
    def create_overhaul_esp(cls, original_esp, overhaul_esp, csids):
        # Change to Data Files directory
        os.chdir(config.mw_data_files_dir)
        # Format the csids into a regex
        csid_regex = cls.get_csid_regex(csids)
        # Run the tes3cmd process
        proc = subprocess.Popen(
            ["tes3cmd.exe", "dump",      # Run the dump command
             "--type", "WEAP",           # Dump both the WEAP type...
             "--type", "ARMO",           # and the ARMO type
             "--type", "CLOT",           # and the CLOT type
             "--id", f"({csid_regex})",  # Which match the list of csids
             "--raw-with-header",        # Output with TES3 header (for new file)
             overhaul_esp,               # Dump to the overhaul .esp (overwrite)
             original_esp],              # Dump from the original .esp
            stdout=subprocess.PIPE,
            shell=True,
            creationflags=subprocess.CREATE_NO_WINDOW)
        (out, err) = proc.communicate()
        # Return to original directory
        os.chdir(cls.original_wd)

    @classmethod
    def get_csid_regex(cls, csids):
        # Format a list of csids for the --id command
        csid_regex = "|".join(csids)
        csid_regex = f"^({csid_regex})$"
        return csid_regex

    @classmethod
    def apply_item_changes(cls, overhaul_esp, overhaul_items, record_type):
        # Change to Data Files directory
        os.chdir(config.mw_data_files_dir)
        for item in overhaul_items:
            # Get the set command
            set_command = cls.get_set_command(record_type, item)
            # Run the tes3cmd process
            proc = subprocess.Popen(
                ["tes3cmd.exe", "modify",  # Run the modify command
                 "--hide-backups",         # Put backups in default directory
                 "--type", record_type,    # Only of the current type
                 "--id", item["csid"],     # Matching the id
                 "--run", set_command,     # Run the set command
                 overhaul_esp],            # On the overhaul .esp
                stdout=subprocess.PIPE,
                shell=True,
                creationflags=subprocess.CREATE_NO_WINDOW)
            (out, err) = proc.communicate()
        # Return to original directory
        os.chdir(cls.original_wd)

    @classmethod
    def get_set_command(cls, record_type, item):
        set_command = ""
        if record_type == "WEAP":
            set_command = cls.get_weapon_set_command(item)
        elif record_type == "ARMO":
            set_command = cls.get_armor_set_command(item)
        elif record_type == "CLOT":
            set_command = cls.get_clothing_set_command(item)
        return set_command

    @classmethod
    def get_weapon_set_command(cls, weapon):
        return f"$R->set({{f=>'weight'}}, {weapon['weight']});" \
                f"$R->set({{f=>'value'}}, {weapon['value']});" \
                f"$R->set({{f=>'health'}}, {weapon['health']});" \
                f"$R->set({{f=>'speed'}}, {weapon['speed']});" \
                f"$R->set({{f=>'reach'}}, {weapon['reach']});" \
                f"$R->set({{f=>'enchantment'}}, {weapon['enchantment']});" \
                f"$R->set({{f=>'chop_min'}}, {weapon['chop_min']});" \
                f"$R->set({{f=>'chop_max'}}, {weapon['chop_max']});" \
                f"$R->set({{f=>'slash_min'}}, {weapon['slash_min']});" \
                f"$R->set({{f=>'slash_max'}}, {weapon['slash_max']});" \
                f"$R->set({{f=>'thrust_min'}}, {weapon['thrust_min']});" \
                f"$R->set({{f=>'thrust_max'}}, {weapon['thrust_max']});"

    @classmethod
    def get_armor_set_command(cls, armor):
        return f"$R->set({{f=>'weight'}}, {armor['weight']});" \
                f"$R->set({{f=>'value'}}, {armor['value']});" \
                f"$R->set({{f=>'health'}}, {armor['health']});" \
                f"$R->set({{f=>'enchantment'}}, {armor['enchantment']});" \
                f"$R->set({{f=>'ar'}}, {armor['ar']});"

    @classmethod
    def get_clothing_set_command(cls, clothing):
        return f"$R->set({{f=>'weight'}}, {clothing['weight']});" \
                f"$R->set({{f=>'value'}}, {clothing['value']});" \
                f"$R->set({{f=>'enchantment'}}, {clothing['enchantment']});"