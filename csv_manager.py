import sys
import os
import config
import csv

class Tes3CsvManager:

    @classmethod
    def dump_to_csvs(cls, record_type, item_dump):
        for esp, dump in item_dump.items():
            cls.create_csv(cls.get_path(esp, record_type, True), dump)
            cls.create_csv(cls.get_path(esp, record_type, False), dump)

    @classmethod
    def get_path(cls, esp, record_type, original):
        if original:
            filename = f"overhaul/original/{esp}_{record_type}_Original.csv"
        else:
            filename = f"overhaul/{esp}_{record_type}_Modified.csv"
        return filename

    @classmethod
    def create_csv(cls, file_path, dump):
        print(f"# Creating {file_path}")
        dump_keys = dump[0].keys()
        with open(file_path, "w", newline="") as csv_file:
            writer = csv.DictWriter(csv_file, dump_keys)
            writer.writeheader()
            writer.writerows(dump)
    
    @classmethod
    def get_modified_records(cls, esp, record_type):
        original_records = cls.get_deserialized_csv(esp, record_type, True)
        modified_records = cls.get_deserialized_csv(esp, record_type, False)
        return cls.get_csv_diff(original_records, modified_records)

    @classmethod
    def get_deserialized_csv(cls, esp, record_type, original):
        csv_path = cls.get_path(esp, record_type, original)
        # Convert .csv to list of dicts
        with open(csv_path) as f:
            reader = csv.DictReader(f)
            res = list(reader)
        return res
    
    @classmethod
    def get_csv_diff(cls, original_records, modified_records):
        diff_records = []
        for orig_record in original_records:
            # Find equivalent modified record based on csid
            mod_record = [mod for mod in modified_records if mod['csid'] == orig_record['csid']][0]
            # If the modified record is different to the original, add it to diff
            if orig_record != mod_record:
                diff_records.append(mod_record)
        # Return None if no diff, else return diff
        if len(diff_records) == 0:
            return None
        else:
            return diff_records
