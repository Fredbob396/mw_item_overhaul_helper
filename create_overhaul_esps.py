import sys
import os
import config
from tes3cmd_interfacer import Tes3cmd
from csv_manager import Tes3CsvManager

# Get all .esp and .esm file names from the run arguments
esps = [a for a in sys.argv if ".esm" in a or ".esp" in a]


def run():
    # Initialize tes3cmd_interfacer with .esp list
    tes3cmd = Tes3cmd(esps)
    for esp in esps:
        # TODO: Generate proper overhaul esp name
        overhaul_esp = f"{esp}_Overhaul.esp"

        print("### Reading modifed records from .csv files")
        overhaul_weapons = Tes3CsvManager.get_modified_records(esp, "WEAP")
        overhaul_armor = Tes3CsvManager.get_modified_records(esp, "ARMO")
        overhaul_clothing = Tes3CsvManager.get_modified_records(esp, "CLOT")

        has_weapons_overhaul = overhaul_weapons is not None
        has_armor_overhaul = overhaul_armor is not None
        has_clothing_overhaul = overhaul_clothing is not None

        # Concatenate all csids for creating the overhaul .esp
        csids = []
        if has_weapons_overhaul:
            csids += [weapon["csid"] for weapon in overhaul_weapons]
        if has_armor_overhaul:
            csids += [armor["csid"] for armor in overhaul_armor]
        if has_clothing_overhaul:
            csids += [clothing["csid"] for clothing in overhaul_clothing]
        
        print(f"### Creating overhaul .esp: {overhaul_esp}")
        # If any record type is overhauled, create the overhaul .esp
        if has_armor_overhaul or has_weapons_overhaul or has_clothing_overhaul:
            tes3cmd.create_overhaul_esp(esp, overhaul_esp, csids)

        print(f"### Applying changes to overhaul .esp: {overhaul_esp}")
        # After the esp is created, apply changes to the overhaul .esp
        if has_weapons_overhaul:
            tes3cmd.apply_item_changes(overhaul_esp, overhaul_weapons, "WEAP")
        if has_armor_overhaul:
            tes3cmd.apply_item_changes(overhaul_esp, overhaul_armor, "ARMO")
        if has_clothing_overhaul:
            tes3cmd.apply_item_changes(overhaul_esp, overhaul_clothing, "CLOT")
        
        print ("### Nuking backups")
        # Nuke the unnecessary tes3cmd backups
        nuke_backups()

        print ("### Done! ###")


def nuke_backups():
    if os.path.exists(config.tes3cmd_backup_dir):
        for filename in os.listdir(config.tes3cmd_backup_dir):
            file_path = os.path.join(config.tes3cmd_backup_dir, filename)
            os.unlink(file_path)


run()
