import sys
from tes3cmd_interfacer import Tes3cmd
from csv_manager import Tes3CsvManager

# Get all .esp and .esm file names from the run arguments
esps = [a for a in sys.argv if ".esm" in a or ".esp" in a]

def run():
    # Initialize tes3cmd_interfacer with .esp list
    print("### Initializing tes3cmd_interfacer")
    tes3cmd = Tes3cmd(esps)

    # Get dump JSON
    print("### Fetching weapons dump")
    weapons_dump = tes3cmd.get_initial_weapons_dump()
    print("### Fetching armor dump")
    armor_dump = tes3cmd.get_initial_armor_dump()
    print("### Fetching clothing dump")
    clothing_dump = tes3cmd.get_initial_clothing_dump()

    # Create record CSVs
    print("### Creating weapon csvs")
    Tes3CsvManager.dump_to_csvs("WEAP", weapons_dump)
    print("### Creating armor csvs")
    Tes3CsvManager.dump_to_csvs("ARMO", armor_dump)
    print("### Creating clothing csvs")
    Tes3CsvManager.dump_to_csvs("CLOT", clothing_dump)
    
    print("### Done! ###")


run()
